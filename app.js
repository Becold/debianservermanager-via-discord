var DiscordClient = require('discord.io');
var ngrok = require('ngrok2');
var util = require('util');

var env = "dev";
var Cfg = require ('./module/cfg/Config.'+env);

var bot = new DiscordClient({
    autorun: Cfg.DiscordClient.autorun,
    email: Cfg.DiscordClient.email,
    password: Cfg.DiscordClient.password
});

bot.on('ready', function() {

    console.log("[Bot] : " + bot.username + " - (" + bot.id + ") is ready.");

});

bot.on('error', function(e) {

    console.log("[Bot][Error] " + e);

});

bot.on('close', function(e) {

    console.log("[Bot][Error] Connection closed: " + e);

});

bot.on('message', function(user, userID, channelID, message, rawEvent) {

    if(userID != Cfg.Me.discord_userId) return;
    console.log("[" + user + "] <<< " + message);

    // Todo: "Routeur" de commande
    if (message === "!ping") {

        bot.sendPrivateMessage(Cfg.Me.discord_userId, "Pong!");

    }
    else if (message === "!help") {

        bot.sendPrivateMessage(Cfg.Me.discord_userId, "Le message d'aide n'est pas encore disponible...");
        // Todo: Message d'aide

    }
    else if (message.split(" ")[0] === "!ngrok"){

        var cmd = getCmd(message, "object");

        ngrok(Cfg.Ngrok.Config).then(function (ng) {

            if (cmd[0] == "start") {

                if(cmd[1] == null || cmd[2] == null){
                    bot.sendPrivateMessage(Cfg.Me.discord_userId, "Le protocole ou le port est manquant");
                    return;
                }

                ng.api.addTunnel({proto: "tcp", addr: cmd[2], name: cmd[1]}).then(function (tunnel) {

                    bot.sendPrivateMessage(Cfg.Me.discord_userId, tunnel.public_url + " --> " + tunnel.config.addr + "\n");

                }, function(e) {

                    bot.sendPrivateMessage(Cfg.Me.discord_userId, "Impossible d'ouvrir un tunnel avec ce port.");

                });
            }
            else if (cmd[0] == "stop") {

                if(cmd[1] == null){
                    bot.sendPrivateMessage(Cfg.Me.discord_userId, "Il manque un paramètre.");
                    return;
                }

                ng.api.stopTunnel(cmd[1]).then(function (tunnels) {
                    var msg = "Tunnel '" + cmd[1] + "' successfully closed.";

                    if(tunnels) {
                        msg += "\n\nThese tunnels still work :\n";

                        for (var x in tunnels) {
                            tunnel = tunnels[x];
                            msg += tunnel.name + ": " + tunnel.public_url + " --> " + tunnel.config.addr + "\n";
                        }
                    }

                    bot.sendPrivateMessage(Cfg.Me.discord_userId, msg);
                });
            }
            else if (cmd[0] == "get"){

                ng.api.getTunnels().then(function (tunnels) {
                    msg = "These tunnels still work :\n";

                    for(var x in tunnels){
                        tunnel = tunnels[x];
                        msg += tunnel.name + ": " + tunnel.public_url + " --> " + tunnel.config.addr + "\n";
                    }

                    bot.sendPrivateMessage(Cfg.Me.discord_userId, msg);
                });
            }
            else {

                bot.sendPrivateMessage(Cfg.Me.discord_userId, "Cette commande n'existe pas");

            }

        });
    }
    else if (message.split(" ")[0] === "!cmd"){

        cmd = getCmd(message, "text");

        var _exec = require('child_process').exec;

        child = _exec(cmd, { encoding: "utf8" }, function (error, stdout, stderr) {
            if(error != null) bot.sendPrivateMessage(Cfg.Me.discord_userId, "Erreur: " + error);
            if(stderr != null) console.log("stderr: " + util.inspect(stderr));

            for (var i = 0; i <= stdout.length / 1950; i++)
                bot.sendPrivateMessage(Cfg.Me.discord_userId, stdout.substring(1950 * i, 1950 * (i + 1)));
        });
    }
    else {

        bot.sendPrivateMessage(Cfg.Me.discord_userId, "[Erreur] Commande '" + message + "' inconnue.");

    }
});

bot.sendPrivateMessage = function(userid, message){
    console.log("[Bot] >>> " + message);

    bot.sendMessage({
        to: userid,
        message: message
    });
};

function getCmd(params, type) {

    if(type == "object") {
        var cmd = [];

        for (var i = 0; i < params.split(" ").length; i++) {
            if (i == 0) continue;

            cmd.push(params.split(" ")[i]);
        }
    }
    else if (type == "text") {
        var cmd = "";

        for (var i = 0; i < params.split(" ").length; i++) {
            if (i == 0) continue;

            if(i == params.split(" ").length-1)
                cmd += params.split(" ")[i];
            else
                cmd += params.split(" ")[i] + " ";
        }
    }

    return cmd;
}