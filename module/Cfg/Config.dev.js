var Cfg = require('./Config.js');

Cfg.DiscordClient.email = "";
Cfg.DiscordClient.password = "";

Cfg.Ngrok.Config.authtoken = "";

Cfg.Me.discord_userId = "";

module.exports = Cfg;