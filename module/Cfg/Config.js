var Cfg = module.exports =

    {
    DiscordClient: {
        autorun: true,
        email: "",
        password: ""
    },
    Ngrok: {
        Config: {
            "authtoken": "",
            "compress_conn": false,
            "http_proxy": false,
            "inspect_db_size": 50000000,
            "metadata": "",
            "root_cas": "trusted",
            "socks5_proxy": "",
            "update": true,
            "update_channel": "stable",
            "web_addr": "127.0.0.1:4040"
        },
        Tunnels: {
            "ssh": {
                name: "ssh",
                addr: "22",
                proto: "tcp"
            },
            "http": {
                name: "http",
                addr: "80",
                proto: "tcp"
            }
        }
    },
    Me: {
        discord_userId: ""
    }
};